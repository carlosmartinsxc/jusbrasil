
export function addRecordToList( baseRecord, linkListName, newRecord) {
    const newListLinkRecords = [...baseRecord.getLinkedRecords(linkListName), newRecord];
    baseRecord.setLinkedRecords(newListLinkRecords, linkListName); //
}

export function updateList( baseRecord, linkListName, newListLinkRecords) {
    baseRecord.setLinkedRecords(newListLinkRecords, linkListName); //
}

export function validateItem(  baseRecord, errorPath, errors) {
    baseRecord.setLinkedRecords(errors, errorPath); //
    console.log(baseRecord,baseRecord.getLinkedRecords(errorPath))
}

export function updateRecordValue(  baseRecord, valuePath, value) {
    baseRecord.setValue(value, valuePath); //
}