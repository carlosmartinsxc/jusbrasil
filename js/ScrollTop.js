import withRouter from "react-router-dom/withRouter";
import React from "react";
class ScrollTop extends React.Component {
    componentDidUpdate(prevProps) {
        if (this.props.location !== prevProps.location) {
            if (this.props.location.pathname.split("/").length === 2) {
                window.scrollTo(0, 0);
            }
        }
    }
    componentDidMount() {}
    render() {
        return this.props.children
    }
}
export default withRouter(ScrollTop)