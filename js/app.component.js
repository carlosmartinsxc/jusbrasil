import React from 'react';
import withStyles from "@material-ui/core/styles/withStyles";
import {HashRouter, Redirect, Route} from "react-router-dom";
import classNames from 'classnames';
import mobile from 'is-mobile';
import ScrollToTop from "./ScrollTop";
import * as Cookies from "js-cookie";
import {graphql, QueryRenderer} from "react-relay";
import StoreApp from "./components/StoreApp";
import {stylesApp} from "./assets/jss/app/store";


class AppComponent extends React.Component {
    render() {
        const {classes, modernEnvironment} = this.props;
        let mobile1 = mobile();
        return (
            <QueryRenderer
                environment={modernEnvironment}
                query={graphql`
            query appQuery {
                user {
                    ...StoreApp_user,
                }
                cart{
                    ...StoreApp_cart,
                }
                products {
                    ...StoreApp_products,
                }
            }
        `}
                variables={{}}
                render={({error, props}) => {
                    let propsQuery = props;
                    if (propsQuery) {
                        return <HashRouter>
                            <ScrollToTop>
                                <div className={classes.root}>
                                    <main
                                        className={classNames(classes.content, mobile1 ? 'mobile' : '')}>
                                        <Route exact path="/" render={pro => (<StoreApp
                                            user={propsQuery.user}
                                            cart={propsQuery.cart}
                                            products={propsQuery.products}
                                        />)}/>
                                        <PrivateRoute exact path="/user"
                                                      render={pro => <StoreApp
                                                          user={propsQuery.user}
                                                          cart={propsQuery.cart}
                                                          products={propsQuery.products}
                                                      />}/>
                                    </main>
                                </div>
                            </ScrollToTop>
                        </HashRouter>
                    } else {
                        return <div>Loading</div>;
                    }
                }}
            />
        );
    }
}

const fakeAuth = {
    isAuthenticated() {
        return true;
        return Cookies.get('login') === "olga";
    },
};


function PrivateRoute({component: Component, ...rest}) {
    return (
        <Route
            {...rest}
            render={props =>
                fakeAuth.isAuthenticated() ? (
                    <Component {...props} />
                ) : (
                    <Redirect
                        to={{
                            pathname: "/login",
                            state: {from: props.location}
                        }}
                    />
                )
            }
        />
    )
};


export default withStyles(stylesApp)(AppComponent);