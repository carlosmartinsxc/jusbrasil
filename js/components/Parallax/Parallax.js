import React from "react";
import classNames from "classnames";
import PropTypes from "prop-types";
import withStyles from "@material-ui/core/styles/withStyles";
import {ParallaxStyle} from "../../assets/jss/app/components/parallaxStyle";
import BackgroundSlideshow from 'react-background-slideshow'

// core components


class Parallax extends React.Component {
    constructor(props) {
        super(props);
        let windowScrollTop = window.pageYOffset / 3;
        this.state = {
            transform: "translate3d(0," + windowScrollTop + "px,0)"
        };
        this.resetTransform = this.resetTransform.bind(this);
    }

    componentDidMount() {
        let windowScrollTop = window.pageYOffset / 3;
        this.setState({
            transform: "translate3d(0," + windowScrollTop + "px,0)"
        });
        window.addEventListener("scroll", this.resetTransform);
    }

    componentWillUnmount() {
        window.removeEventListener("scroll", this.resetTransform);
    }

    resetTransform() {
        let windowScrollTop = window.pageYOffset / 3;
        this.setState({
            transform: "translate3d(0," + windowScrollTop + "px,0)"
        });
    }

    render() {
        const {
            classes,
            filter,
            className,
            children,
            style,
            images,
            small
        } = this.props;
        const parallaxClasses = classNames({
            [classes.parallax]: true,
            [classes.filter]: filter,
            [classes.small]: small,
            [className]: className !== undefined
        });
        return (

            <div
                className={parallaxClasses}
                style={{
                    ...style,
                }}
                ref="parallax"
            >
                <div style={{opacity: "0.8"}}><BackgroundSlideshow images={images}/></div>


                {children}
            </div>
        );
    }
}

Parallax.propTypes = {
    classes: PropTypes.object.isRequired,
    className: PropTypes.string,
    filter: PropTypes.bool,
    children: PropTypes.node,
    style: PropTypes.string,
    images: PropTypes.array
};

export default withStyles(ParallaxStyle)(Parallax);
