import React from 'react';
import ProductCard from './ProductCard';
import Grid from '@material-ui/core/Grid';
import * as PropTypes from "prop-types";
import withStyles from "@material-ui/core/es/styles/withStyles";
import {StoreAppStyle} from "../../assets/jss/app/store";
import {createFragmentContainer} from "react-relay";
import {shallowEqual} from "recompose";

const styles = theme => ({
    container: {},
    fab: {
        textAlign: 'center'
    },
    filterRegion: {
        maxWidth: '500px',
        margin: '0 auto'
    },
    extendedIcon: {
        marginRight: theme.spacing.unit,
    },
});

class ProductList extends React.Component {
    shouldComponentUpdate(nextProps, nextState, nextContext) {
        return !shallowEqual(nextProps.products,this.props.products)
    }

    render() {
        const {products,user,  relay} = this.props;
        return (
            <div>
                <Grid container justify="center" spacing={24}>
                    {products.map((product, i) => {
                        return (
                            <Grid key={i} item lg={6} xs={12} sm={6} md={6}>
                                <ProductCard
                                    product={product}
                                    user={user}
                                    relay={relay}
                                />
                            </Grid>)
                    })}
                </Grid>
            </div>
        );
    }
}

ProductList.propTypes = {
    products: PropTypes.array,
    user: PropTypes.object.isRequired,
    maxGenerate: PropTypes.number,
    relay: PropTypes.object.isRequired,
};

export default createFragmentContainer(withStyles(StoreAppStyle)(ProductList), {
    products: graphql`
        fragment ProductList_products on Product @relay(plural: true) {
            ...ProductCard_product,
            ...CommentList_product
        }
    `,
});