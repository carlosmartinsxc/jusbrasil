import * as React from "react";
import PropTypes from 'prop-types';
import {withStyles} from '@material-ui/core/styles';
import classnames from 'classnames';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardMedia from '@material-ui/core/CardMedia';
import CardContent from '@material-ui/core/CardContent';
import CardActions from '@material-ui/core/CardActions';
import Collapse from '@material-ui/core/Collapse';
import Avatar from '@material-ui/core/Avatar';
import IconButton from '@material-ui/core/IconButton';
import red from '@material-ui/core/colors/red';
import FavoriteIcon from '@material-ui/icons/Favorite';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import CommentIcon from '@material-ui/icons/Comment';
import MoreVertIcon from '@material-ui/icons/MoreVert';
import Grid from "@material-ui/core/Grid/Grid";
import Chip from "@material-ui/core/Chip/Chip";
import {formatMoney} from "../../app";
import Badge from "@material-ui/core/Badge/Badge";
import DecreaseCartItemMutation from "../ShoppingCart/mutation/DecreaseCartItemMutation";
import ToggleFavoriteMutation from "./mutations/ToggleFavoriteMutation";
import CommentList from "../Comments/CommentList";
import {createFragmentContainer} from "react-relay";
import ProductShoppingCartButton from "./ProductShoppingCartButton";
import {shallowEqual} from "recompose";

const styles = theme => ({
    card: {
        maxWidth: 400,
        margin: '0 auto',
    },
    media: {
        height: 0,
        paddingTop: '56.25%', // 16:9
    },
    actions: {
        display: 'flex',
    }, priceChip: {
        float: 'right',
    },
    expand: {
        transform: 'rotate(0deg)',
        marginLeft: 'auto',
        transition: theme.transitions.create('transform', {
            duration: theme.transitions.duration.shortest,
        }),
    },
    expandOpen: {
        transform: 'rotate(180deg)',
    },
    avatar: {
        backgroundColor: red[500],
    },
});


class ProductCard extends React.Component {
    state = {expanded: false, commentExpanded: false};
    _handleDecreaseItem = product => {
        DecreaseCartItemMutation.commit(
            this.props.relay.environment,
            product.id,
        );
    };
    _handleToggleFavorite = product => {
        ToggleFavoriteMutation.commit(
            this.props.relay.environment,
            product.id,
        );
    };
    handleExpandClick = () => {
        this.setState(state => ({expanded: !state.expanded}));
    };
    handleCommentExpandClick = () => {
        this.setState(state => ({commentExpanded: !state.commentExpanded}));
    };

    shouldComponentUpdate(nextProps, nextState, nextContext) {
        return !shallowEqual(nextProps.product,this.props.product)
        || !shallowEqual(nextState,this.state)
    }

    render() {
        const {classes, relay, user, product} = this.props;
        const {anchorEl} = this.state;
        const open = Boolean(anchorEl);
        const loveSelect = product.favorite;
        return (
            <Card className={classes.card}>
                <CardHeader
                    avatar={
                        <Avatar
                            src={product.owner.avatar}
                            aria-label={product.owner.avatar} className={classes.avatar}>
                        </Avatar>
                    }
                    action={
                        <IconButton>
                            <MoreVertIcon/>
                        </IconButton>
                    }
                    title={product.title}
                    subheader={product.owner.fullName}
                />
                <CardMedia
                    className={classes.media}
                    image={product.images[0]}
                    title={product.title}
                />
                <CardContent>
                    <Grid container={true} spacing={8} justify={"space-between"}>
                        <Grid item xs={6} container={true} alignContent={"flex-start"}>
                            <Chip
                                label={product.description}
                                color="secondary" variant="outlined"/>
                        </Grid>
                        <Grid item xs={6}>
                            <Chip
                                className={classes.priceChip}
                                label={formatMoney(product.price)}
                                color="primary"
                                variant="default"/>
                        </Grid>
                    </Grid>
                </CardContent>
                <CardActions className={classes.actions} disableActionSpacing>
                    <IconButton color={loveSelect ? "secondary" : "default"}
                                onClick={() => this._handleToggleFavorite(product)} aria-label="Add to favorites">
                        <FavoriteIcon/>
                    </IconButton>
                    <ProductShoppingCartButton
                        product={this.props.product}
                    />
                    <IconButton
                        color={this.state.commentExpanded ? "primary" : "default"}
                        onClick={this.handleCommentExpandClick}
                        aria-expanded={this.state.commentExpanded}
                        aria-label="Show more"
                        className={classnames(classes.expand)}
                    >
                        <Badge className={classes.margin} badgeContent={product.totalComments} color="default">
                            <CommentIcon/>
                        </Badge>
                    </IconButton>
                    <IconButton
                        className={classnames(classes.expand, {
                            [classes.expandOpen]: this.state.expanded,
                        })}
                        onClick={this.handleExpandClick}
                        aria-expanded={this.state.expanded}
                        aria-label="Show more"
                    >
                        <ExpandMoreIcon/>
                    </IconButton>
                </CardActions>
                <Collapse in={this.state.expanded} timeout="auto" unmountOnExit>
                    <CardContent>
                        <Chip
                            label={`Stock: ${product.stock}`}
                            color="default" variant="outlined"/>
                        <Chip
                            avatar={<Avatar
                                src={product.owner.avatar}
                                aria-label={product.owner.avatar} className={classes.avatar}>
                            </Avatar>}
                            label={`${product.owner.fullName}`}
                            color="default" variant="outlined"/>
                    </CardContent>
                </Collapse>
                <Collapse in={this.state.commentExpanded} timeout="auto" unmountOnExit>
                    <CommentList
                        relay={relay}
                        user={user}
                        product={product}
                    />

                </Collapse>
            </Card>
        );
    }
}

ProductCard.propTypes = {
    classes: PropTypes.object.isRequired,
    product: PropTypes.object,
};

export default createFragmentContainer(withStyles(styles)(ProductCard), {
    product: graphql`
        fragment ProductCard_product on Product @relay(mask: false){
            id,
            title,
            owner{
                avatar,
                fullName
            },
            description,
            images,
            material,
            stock,
            price,
            favorite,
            totalComments,
            ...CommentList_product
            ...ProductShoppingCartButton_product
        }
    `,
});