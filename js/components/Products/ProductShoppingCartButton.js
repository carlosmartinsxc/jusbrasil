import React from 'react';
import Badge from "@material-ui/core/Badge/Badge";
import ShoppingCartIcon from '@material-ui/icons/ShoppingCart';
import withStyles from "@material-ui/core/es/styles/withStyles";
import {createFragmentContainer} from "react-relay";
import AddCartItemMutation from "../ShoppingCart/mutation/AddCartItemMutation";
import IconButton from "@material-ui/core/IconButton/IconButton";
import ExposurePlus1Icon from '@material-ui/icons/ExposurePlus1';
import ExposureNeg1Icon from '@material-ui/icons/ExposureNeg1';
import DecreaseCartItemMutation from "../ShoppingCart/mutation/DecreaseCartItemMutation";

const styles = theme => ({});


class ProductShoppingCartButton extends React.Component {

    _handleAddItem = product => {
        AddCartItemMutation.commit(
            this.props.relay.environment,
            product.id,
        );
    };

    _handleDecreaseItem = product => {
        DecreaseCartItemMutation.commit(
            this.props.relay.environment,
            product.id,
        );
    };


    render() {
        const {classes, product} = this.props;
        return (
            <div>
                <IconButton aria-label="Buy"
                            color={product.totalInCart ? "secondary" : "default"}
                            onClick={() => this._handleAddItem(product)}>
                    <Badge className={classes.margin} badgeContent={product.totalInCart} color="primary">
                        {product.totalInCart > 0 && <ExposurePlus1Icon/>}
                        {product.totalInCart === 0 && <ShoppingCartIcon/>}
                    </Badge>
                </IconButton>
                {product.totalInCart > 1 &&
                <IconButton color={"default"}
                            onClick={() => this._handleDecreaseItem(product)} aria-label="Add to favorites">
                    <ExposureNeg1Icon/>
                </IconButton>
                }
            </div>
        );
    }
}

ProductShoppingCartButton.propTypes = {};

export default createFragmentContainer(withStyles(styles)(ProductShoppingCartButton), {
    product: graphql`
        fragment ProductShoppingCartButton_product on Product @relay(mask: false){
            id,
            totalInCart
        }
    `,
});