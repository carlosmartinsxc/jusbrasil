import {commitMutation, graphql} from 'react-relay';
import {updateRecordValue} from "../../../mutations/mutation.helpers";

// language=GraphQL
const mutation = graphql`
    mutation ToggleFavoriteMutation($input: ToggleFavoriteMutationInput!) {
        toggleFavorite(input: $input) {
            clientMutationId,
            userEdge {
                id,
                favorites,
            }
            productEdge {
                favorite,
            }
        }
    }
`;
let tempID = 0;

function commit(environment, productId) {
    return commitMutation(environment, {
        mutation,
        variables: {
            input: {
                productId,
                clientMutationId: String(tempID++),
            },
        },
        updater: (store) => {
            const rootField = store.getRootField('toggleFavorite');
            const user = rootField.getLinkedRecord('userEdge');
            const productEdge = rootField.getLinkedRecord('productEdge');
            updateRecordValue(
                user,
                'favorites',
                user.getValue('favorites'));
            updateRecordValue(
                store.get(productId),
                'favorite',
                productEdge.getValue('favorite'));

        }
    });
}

export default {commit};
