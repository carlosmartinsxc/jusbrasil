import * as React from "react";
import {withStyles} from '@material-ui/core/styles';
import classNames from "classnames";
import Typist from "react-typist";
import Parallax from "./Parallax/Parallax";
import ProductsList from "./Products/ProductList";
import {StoreAppStyle} from "../assets/jss/app/store";
import Header from "./Header/Header";
import HeaderLinks from "./Header/HeaderLinks";
import {createFragmentContainer} from "react-relay";
import faker from 'faker';
import Grid from "@material-ui/core/Grid/Grid";

class Store extends React.Component {
    constructor(props) {
        super(props);
    }

    state = {
        skip: false
    };

    skipContainer = () => {
        this.setState({skip: true})
    }


    render() {
        const {classes, cart, relay, ...rest} = this.props;

        return (
            <div className={classes.root}>
                <Header
                    rightLinks={<HeaderLinks/>}
                    cart={cart}
                    relay={relay}
                    fixed
                    changeColorOnScroll={{
                        height: 400,
                        color: "white"
                    }}
                />
                <Parallax filter images={[
                    require("../assets/img/bg/001.jpg"),
                    require("../assets/img/bg/002.jpg"),
                    require("../assets/img/bg/003.jpg")
                ]}>
                    <div className={classes.containerHeader} onClick={this.skipContainer}>
                        <Grid container>
                            <Grid item  xs={12} className={classes.titleRegion}>
                                <h5 className={classes.paralaxTitle}>
                                    <Typist
                                        avgTypingDelay={60}
                                        startDelay={2300}
                                        cursor={{show: false}}>
                                        {this.getTitle()}
                                    </Typist>
                                </h5>
                            </Grid>
                        </Grid>
                    </div>
                </Parallax>
                <div className={classNames(classes.mainCenter, classes.sectionProducts, classes.sectionProductsFirst)}>
                    <ProductsList
                        maxGenerate={10}
                        title={"Funcionalidades"}
                        products={this.props.products}
                        user={this.props.user}
                        relay={this.props.relay}
                        type={"funcionalidade"}
                    />
                </div>
            </div>
        );
    }


    getTitle() {
        return <span>  Welcome ...<br/>
              <Typist.Backspace count={3} delay={1000}/>
            {faker.company.catchPhrase()}...<br/>
              <Typist.Backspace count={3} delay={1000}/>
            {faker.company.catchPhrase()}...<br/>
              <Typist.Backspace count={3} delay={1000}/>
            {faker.company.catchPhrase()}...<br/>
              <Typist.Backspace count={3} delay={1000}/>
            {faker.company.catchPhrase()}...<br/>
        </span>;
    }

}

const StoreApp = withStyles(StoreAppStyle)(Store);
export default createFragmentContainer(StoreApp, {
    cart: graphql`
        fragment StoreApp_cart on ShoppingCart{
            id,
            products{
                ...ShoppingCartList_products
            },
            ...ShoppingCartButton_cart,
        }
    `,
    user: graphql`
        fragment StoreApp_user on User {
            id,
            avatar,
            name,
            favorites,
            likeComments,
        }
    `,
    products: graphql`
        fragment StoreApp_products on Product @relay(plural: true){
            ...ProductList_products
        }
    `,
});