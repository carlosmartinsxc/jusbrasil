/*eslint-disable*/
import React from "react";
import {withRouter} from "react-router-dom";
import withStyles from "@material-ui/core/styles/withStyles";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import Tooltip from "@material-ui/core/Tooltip";
import LocalAtm from '@material-ui/icons/LocalAtm';

import Button from "@material-ui/core/Button/Button";
import {headerLinksStyle} from "../../assets/jss/app/components/headerLinksStyle";

function HeaderLinks({...props}) {
    const {classes, shoppingCartButton} = props;
    return (
        <List className={classes.list}>
            <ListItem className={classes.listItem}>
                <Button
                    to="/products"
                    selected={props.location.pathname === "/products"}
                    className={classes.navLink}
                >
                    <LocalAtm className={classes.icons}/> Products
                </Button>
            </ListItem>

            <ListItem className={classes.listItem}>
                {shoppingCartButton}
            </ListItem>

            <ListItem className={classes.listItem}>
                <Tooltip
                    id="instagram-facebook"
                    title="Facebook"
                    placement={window.innerWidth > 959 ? "top" : "left"}
                    classes={{tooltip: classes.tooltip}}
                >
                    <Button
                        href="https://www.facebook.com/unmaze.io/"
                        target="_blank"
                        className={classes.navLink}
                    >
                        <i className={classes.socialIcons + " fab fa-facebook"}/>
                    </Button>
                </Tooltip>
            </ListItem>
            <ListItem className={classes.listItem}>
                <Tooltip
                    id="instagram-tooltip"
                    title="Youtube"
                    placement={window.innerWidth > 959 ? "top" : "left"}
                    classes={{tooltip: classes.tooltip}}
                >
                    <Button
                        href="https://www.youtube.com/channel/UC0ePCC7W9Np8N5xynpjzYxw"
                        target="_blank"
                        className={classes.navLink}
                    >
                        <i className={classes.socialIcons + " fab fa-youtube"}/>
                    </Button>
                </Tooltip>
            </ListItem>
        </List>
    );
}

export default withStyles(headerLinksStyle)(withRouter(HeaderLinks));
