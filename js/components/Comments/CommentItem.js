import React from 'react';
import PropTypes from 'prop-types';
import {withStyles} from '@material-ui/core/styles';
import IconButton from "@material-ui/core/IconButton/IconButton";
import Avatar from "@material-ui/core/Avatar/Avatar";
import ToggleCommentLikeMutation from "./mutations/ToggleCommentLikeMutation";
import ListItemAvatar from "@material-ui/core/ListItemAvatar/ListItemAvatar";
import ListItem from "@material-ui/core/ListItem/ListItem";
import ListItemText from "@material-ui/core/ListItemText/ListItemText";
import ListItemSecondaryAction from "@material-ui/core/ListItemSecondaryAction/ListItemSecondaryAction";
import ThumbUpAltIcon from '@material-ui/icons/ThumbUpAlt';
import {createFragmentContainer} from "react-relay";

const styles = theme => ({
    root: {
        flexGrow: 1,
    },
    paper: {
        padding: theme.spacing.unit * 2,
        margin: 'auto',
        maxWidth: 500,
    },
    appBar: {
        top: 'auto',
        bottom: 0,
    },
    image: {
        width: "100%",
        height: "100%",
    },
    img: {
        margin: 'auto',
        display: 'block',
        maxWidth: '100%',
        maxHeight: '100%',
    },
});

export class CommentItem extends React.Component {
    _handleLike = comment => {
        ToggleCommentLikeMutation.commit(
            this.props.relay.environment,
            comment.id,
        );
    };


    render() {
        const {classes, comment} = this.props;
        let like = comment.likeComment;
        return (<ListItem alignItems="flex-start">
                <ListItemAvatar>
                    <Avatar src={comment.editor.avatar}/>
                </ListItemAvatar>
                <ListItemText
                    primary={comment.editor.fullName}
                    secondary={
                        <React.Fragment>
                            {comment.comment}
                        </React.Fragment>
                    }
                />
                <ListItemSecondaryAction>
                    <IconButton color={like ? "primary" : "default"} onClick={() => this._handleLike(comment)}
                                aria-label="Comments">
                        <ThumbUpAltIcon/>
                    </IconButton>
                </ListItemSecondaryAction>
            </ListItem>
        );
    }
}

CommentItem.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default createFragmentContainer(withStyles(styles)(CommentItem), {
    comment: graphql`
        fragment CommentItem_comment on Comment @relay(mask:false) {
            editor{
                fullName,
                avatar
            },
            errors{
                field,
                message
            },
            likeComment,
            id,
            comment
        }
    `,
});