import {commitMutation, graphql} from 'react-relay';
import {updateRecordValue, validateItem} from "../../../mutations/mutation.helpers";

// language=GraphQL
const mutation = graphql`
    mutation ToggleCommentLikeMutation($input: ToggleCommentLikeMutationInput!) {
        toggleCommentLike(input: $input) {
            clientMutationId,
            commentEdge {
                id,
                likeComment,
            }
        }
    }
`;
let tempID = 0;

function commit(environment, commentId) {
    return commitMutation(environment, {
        mutation,
        variables: {
            input: {
                commentId,
                clientMutationId: String(tempID++),
            },
        },
        updater: (store) => {
            const rootField = store.getRootField('toggleCommentLike');
            const comment = rootField.getLinkedRecord('commentEdge');
            const errors = comment.getLinkedRecords('errors');
            if (errors && errors.length) {
                validateItem(
                    store.get(commentId),
                    'errors',
                    errors);
            } else {
                updateRecordValue(
                    store.get(commentId),
                    'likeComment',
                    comment.getValue('likeComment'));
            }
        }
    });
}

export default {commit};
