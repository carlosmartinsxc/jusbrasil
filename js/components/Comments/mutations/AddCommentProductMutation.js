import {commitMutation, graphql} from 'react-relay';
import {ConnectionHandler} from 'relay-runtime';
import {addRecordToList, updateRecordValue, validateItem} from "../../../mutations/mutation.helpers";

// language=GraphQL
const mutation = graphql`
    mutation AddCommentProductMutation($input: AddCommentProductMutationInput!) {
        addCommentProduct(input: $input) {
            clientMutationId,
            commentEdge {
                ...CommentItem_comment
            },
        }
    }
`;
let tempID = 0;


function commit(environment, text, productId) {

    return commitMutation(environment, {
        mutation,
        variables: {
            input: {
                text,
                productId,
                clientMutationId: String(tempID++),
            },
        },
        updater: (store) => {
            const rootField = store.getRootField('addCommentProduct');
            const comment = rootField.getLinkedRecord('commentEdge');
            const errors = comment.getLinkedRecords('errors');
            if (errors && errors.length) {
                validateItem(
                    store.get(productId),
                    'commentErrors',
                    errors);
            } else {
                validateItem(
                    store.get(productId),
                    'commentErrors',
                    []);
                addRecordToList(
                    store.get(productId),
                    'comments',
                    comment);
                updateRecordValue(
                    store.get(productId),
                    'totalComments',
                    store.get(productId).getValue('totalComments') + 1);
            }
        }
    });
}

export default {commit};
