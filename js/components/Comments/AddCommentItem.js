import React from 'react';
import PropTypes from 'prop-types';
import {withStyles} from '@material-ui/core/styles';
import IconButton from "@material-ui/core/IconButton/IconButton";
import Avatar from "@material-ui/core/Avatar/Avatar";
import ListItemAvatar from "@material-ui/core/ListItemAvatar/ListItemAvatar";
import ListItem from "@material-ui/core/ListItem/ListItem";
import ListItemText from "@material-ui/core/ListItemText/ListItemText";
import ListItemSecondaryAction from "@material-ui/core/ListItemSecondaryAction/ListItemSecondaryAction";
import SendIcon from '@material-ui/icons/Send';
import TextField from "@material-ui/core/TextField/TextField";
import AddCommentProductMutation from "./mutations/AddCommentProductMutation";
import {createFragmentContainer} from "react-relay";
import Badge from "@material-ui/core/Badge/Badge";

const styles = theme => ({
    root: {
        flexGrow: 1,
        position: 'sticky',
        zIndex: 1,
        bottom: 0,
        background: theme.palette.common.white,
        borderTop: "1px solid rgba(0, 0, 0, 0.2)",
    },
    paper: {
        padding: theme.spacing.unit * 2,
        margin: 'auto',
        maxWidth: 500,
    },
    textField: {
        marginLeft: theme.spacing.unit,
        marginRight: theme.spacing.unit,
        width: 200,
    },
    image: {
        width: "100%",
        height: "100%",
    }, hidden: {
        display: 'none'
    },
    img: {
        margin: 'auto',
        display: 'block',
        maxWidth: '100%',
        maxHeight: '100%',
    },
});

class AddCommentItem extends React.Component {
    state = {text: '', sentText: null};
    handleChange = event => {
        this.setState({
            text: event.target.value,
        });
    };
    addComment = (e) => {
        AddCommentProductMutation.commit(
            this.props.relay.environment,
            this.state.text,
            this.props.product.id
        );
        this.setState({text: '', sentText: this.state.text})
        return false;//preventSubmit
    };

    render() {
        const {classes, product, user} = this.props;
        let commentErrors = product.commentErrors;
        let errorLabel = commentErrors ? commentErrors.map(error => error.message).join(",") : '';
        return (<ListItem alignItems="flex-start" classes={{
                container: classes.root
            }}>
                <ListItemAvatar>
                    <Avatar src={user.avatar}/>
                </ListItemAvatar>
                <form onSubmit={this.addComment}>
                <ListItemText
                    disableTypography={true}
                    secondary={
                        <TextField
                            helperText={errorLabel}
                            error={!!commentErrors}
                            rowsMax="2"
                            value={this.state.text}
                            onChange={this.handleChange}
                            className={classes.textField}
                            margin="normal"
                        />
                    }
                />
                </form>
                <ListItemSecondaryAction>
                    <IconButton onClick={() => this.addComment()}
                                aria-label="Comments">
                        <Badge className={classes.margin} badgeContent={commentErrors ? commentErrors.length : ''}
                               color="default">
                            <SendIcon/>
                        </Badge>
                    </IconButton>
                </ListItemSecondaryAction>
            </ListItem>
        );
    }
}

AddCommentItem.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default createFragmentContainer(withStyles(styles)(AddCommentItem), {
    product: graphql`
        fragment AddCommentItem_product on Product{
            id
            commentErrors{
                field,
                message
            }
        }
    `,
});