import React from 'react';
import * as PropTypes from "prop-types";
import withStyles from "@material-ui/core/es/styles/withStyles";
import CommentItem from "./CommentItem";
import List from "@material-ui/core/List/List";
import {createFragmentContainer} from "react-relay";
import AddCommentItem from "./AddCommentItem";

const styles = theme => ({
    container: {},
    root: {
        width: '100%',
        maxHeight: 342,
        overflowY: 'auto',
        backgroundColor: theme.palette.background.paper,
    },
});

class CommentList extends React.Component {
    render() {
        console.log(this.props)
        const {classes, relay, user, product} = this.props;
        return (
            <List className={classes.root}>
                {product.comments.map((comment, i) => {
                    return (
                        <CommentItem
                            key={i}
                            comment={comment}
                            relay={relay}
                        />
                    )
                })}
                <AddCommentItem
                    user={user}
                    product={product}
                    relay={relay}
                />
            </List>
        );
    }
}

CommentList.propTypes = {
    product: PropTypes.object.isRequired,
    user: PropTypes.object.isRequired,
    relay: PropTypes.object.isRequired,
};

export default createFragmentContainer(withStyles(styles)(CommentList), {
    product: graphql`
        fragment CommentList_product on Product {
            id,
            comments{
                ...CommentItem_comment
            },
            ...AddCommentItem_product
        }
    `,
});