import React from 'react';
import PropTypes from 'prop-types';
import {withStyles} from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import ShoppingCartItem from "./ShoppingCartItem";
import Toolbar from "@material-ui/core/Toolbar/Toolbar";
import AppBar from "@material-ui/core/AppBar/AppBar";
import Typography from "@material-ui/core/Typography/Typography";
import Button from "@material-ui/core/Button/Button";
import {formatMoney} from "../../app";
import {createFragmentContainer} from "react-relay";

const styles = theme => ({
    root: {
        flexGrow: 1,
    },
    paper: {
        margin: 'auto',
        overflow: 'hidden',
        maxWidth: 500,
        minWidth: 400,
    },
    barBottom: {
        position: 'absolute',
        zIndex: 1,
        bottom: 0,
        left: 0,
        height: 50,
        right: 0,
        margin: '0 auto',
    },
    buttonTotalCart: {
        position: "absolute",
        right: 16
    },
    image: {
        width: 128,
        height: 128,
    },
    img: {
        margin: 'auto',
        display: 'block',
        maxWidth: '100%',
        maxHeight: '100%',
    },
});


function ShoppingCartList(props) {
    const {classes, qnt, relay, totalPrice, products} = props;

    return (
        <div className={classes.root}>
            <div className={classes.paper}>

                <AppBar position="sticky" color="default">
                    <Toolbar>
                        <Typography variant="h6" color="inherit">
                            Carrinho de Compras
                        </Typography>
                        {products.length > 0 && <Button
                            variant={"contained"}
                            color={"primary"}
                            className={classes.buttonTotalCart}
                        >
                            {formatMoney(totalPrice)}
                        </Button>
                        }
                    </Toolbar>
                </AppBar>
                <Grid container>
                    {products.map((storeProduct, i) => {
                        return (
                            <ShoppingCartItem
                                key={i}
                                qnt={storeProduct.qnt}
                                product={storeProduct.product}
                                relay={relay}
                            />
                        )
                    })}
                </Grid>
            </div>
        </div>
    );
}

ShoppingCartList.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default createFragmentContainer(withStyles(styles)(ShoppingCartList), {
    products: graphql`
        fragment ShoppingCartList_products on ShoppingCartProduct @relay(plural: true) {
            product{
                ...ShoppingCartItem_product,
            },
            qnt
        }
    `,
});