import React from 'react';
import Button from "@material-ui/core/Button/Button";
import Badge from "@material-ui/core/Badge/Badge";
import ShoppingCartIcon from '@material-ui/icons/ShoppingCart';
import withStyles from "@material-ui/core/es/styles/withStyles";
import ShoppingCartList from "./ShoppingCartList";
import Popover from '@material-ui/core/Popover';
import {headerLinksStyle} from "../../assets/jss/app/components/headerLinksStyle";
import {createFragmentContainer} from "react-relay";

export class ShoppingCartButton extends React.Component {
    state = {
        anchorEl: null,
    };

    handleClick = event => {
        this.setState({
            anchorEl: event.currentTarget,
        });
    };

    handleClose = () => {
        this.setState({
            anchorEl: null,
        });
    };

    render() {
        const {classes, relay, cart} = this.props;
        const {anchorEl} = this.state;
        const open = Boolean(anchorEl);
        let badgeContent = cart.totalItems;
        return (
            <div>
                <Button
                    aria-owns={open ? 'simple-popper' : undefined}
                    aria-haspopup="true"
                    className={classes.navLink}
                    onClick={this.handleClick}
                >
                    <Badge className={classes.margin} badgeContent={badgeContent} color="primary">
                        <ShoppingCartIcon/>
                    </Badge>
                </Button>
                <Popover
                    id="simple-popper"
                    open={open}
                    anchorEl={anchorEl}
                    onClose={this.handleClose}
                    anchorOrigin={{
                        vertical: 'bottom',
                        horizontal: 'right',
                    }}
                    transformOrigin={{
                        vertical: 'top',
                        horizontal: 'right',
                    }}
                >
                    <ShoppingCartList
                        products={cart.products}
                        totalPrice={cart.totalPrice}
                        relay={relay}
                    />
                </Popover>
            </div>
        );
    }
}

ShoppingCartButton.propTypes = {};

export default createFragmentContainer(withStyles(headerLinksStyle)(ShoppingCartButton), {
    cart: graphql`
        fragment ShoppingCartButton_cart on ShoppingCart {
            id,
            totalPrice,
            totalItems,
            products{
                ...ShoppingCartList_products
            },
        }
    `,
});