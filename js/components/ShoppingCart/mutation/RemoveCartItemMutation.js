import {commitMutation, graphql} from 'react-relay';
import {ConnectionHandler} from 'relay-runtime';
import {updateList, updateRecordValue} from "../../../mutations/mutation.helpers";

// language=GraphQL
const mutation = graphql`
    mutation RemoveCartItemMutation($input: RemoveCartItemInput!) {
        removeCartItem(input: $input) {
            clientMutationId,
            cartEdge {
                ...StoreApp_cart,
                products{
                    product{
                        ...ProductShoppingCartButton_product
                    }
                }
            }
        }
    }
`;

let tempID = 0;

function commit(environment, productId) {
    return commitMutation(environment, {
        mutation,
        variables: {
            input: {
                productId,
                clientMutationId: String(tempID++),
            },
        },
        updater: (store) => {
            const rootField = store.getRootField('removeCartItem');
            const cart = rootField.getLinkedRecord('cartEdge');
            updateList(
                cart,
                'products',
                cart.getLinkedRecords('products'));
            let product = store.get(productId);
            updateRecordValue(
                product,
                'totalInCart',
                0);
        }
    });
}

export default {commit};
