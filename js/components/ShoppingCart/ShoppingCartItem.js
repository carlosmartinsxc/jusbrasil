import React from 'react';
import PropTypes from 'prop-types';
import {withStyles} from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import ButtonBase from '@material-ui/core/ButtonBase';
import IconButton from "@material-ui/core/IconButton/IconButton";
import DeleteIcon from '@material-ui/icons/Delete';
import RemoveCartItemMutation from "./mutation/RemoveCartItemMutation";
import {createFragmentContainer} from "react-relay";
import ProductShoppingCartButton from "../Products/ProductShoppingCartButton";
import {formatMoney} from "../../app";

const styles = theme => ({
    root: {
        flexGrow: 1,
    },
    paper: {
        padding: theme.spacing.unit * 2,
        margin: 'auto',
        maxWidth: 500,
    },
    appBar: {
        top: 'auto',
        bottom: 0,
    },
    image: {
        width: "100%",
        height: "100%",
    },
    img: {
        margin: 'auto',
        display: 'block',
        maxWidth: '100%',
        maxHeight: '100%',
    },
});

export class ShoppingCartItem extends React.Component {
    _handleDelete = product => {
        RemoveCartItemMutation.commit(
            this.props.relay.environment,
            product.id,
        );
    };

    render() {
        const {classes, product, relay, qnt} = this.props;
        let image = product.images[0];// ;
        let totalMoney = formatMoney(product.price * qnt);
        let explainMoney = formatMoney(product.price) + ' * ' + qnt;
        return (<Grid container spacing={16}>
                <Grid xs={3} item>
                    <ButtonBase className={classes.image}>
                        <img className={classes.img} alt="complex" src={image}/>
                    </ButtonBase>
                </Grid>
                <Grid xs={9} justify={"space-between"} item container>
                    <Grid item xs={6} direction="column" spacing={0}>
                        <Typography gutterBottom variant="subtitle1">
                            {product.title}
                        </Typography>
                        <Typography variant={"subtitle2"}>{`${totalMoney}`}</Typography>
                        {qnt > 1 && <Typography variant={"caption"}>{`(${explainMoney})`}</Typography>}
                    </Grid>
                    <Grid xs={6} item justify={"space-between"}>
                        <Grid container justify={"space-between"}>
                            <Grid>
                                <ProductShoppingCartButton
                                    relay={relay}
                                    product={product}
                                />
                            </Grid>
                            <Grid>
                                <IconButton onClick={() => this._handleDelete(product)} aria-label="Delete"
                                            className={classes.margin}>
                                    <DeleteIcon fontSize="small"/>
                                </IconButton>
                            </Grid>
                        </Grid>
                    </Grid>
                </Grid>
            </Grid>
        );
    }
}

ShoppingCartItem.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default createFragmentContainer(withStyles(styles)(ShoppingCartItem), {
    product: graphql`
        fragment ShoppingCartItem_product on Product {
            id,
            title,
            owner{
                avatar,
                fullName
            },
            description,
            images,
            material,
            stock,
            price,
            ...ProductShoppingCartButton_product,
        }
    `,
});