import 'todomvc-common';

import React from 'react';
import ReactDOM from 'react-dom';

import {Environment, Network, RecordSource, Store} from 'relay-runtime';
import AppComponent from "./app.component";


function fetchQuery(operation, variables) {
    return fetch('/graphql', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify({
            query: operation.text,
            variables,
        }),
    }).then(response => {
        return response.json();
    });
}

export const formatMoney = function (value) {
    return parseFloat(value).toLocaleString("pt-BR", {currency: "BRL", style: "currency"});
};

const modernEnvironment = new Environment({
    network: Network.create(fetchQuery),
    store: new Store(new RecordSource()),
});

// language=GraphQL
ReactDOM.render(
    <AppComponent modernEnvironment={modernEnvironment}/>,
    document.getElementById('root'),
);
