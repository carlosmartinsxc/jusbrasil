import {grey} from "@material-ui/core/colors/index";
import lightGreen from "@material-ui/core/colors/lightGreen";

const productCardStyle = (theme) => ({
    card: {
        maxWidth: 345,
        display: 'flex',
        flexDirection: 'column',
    },
    mediaImage: {
        width: "100%",
    },
    mediaDialog: {
        maxWidth: "100%",
        width: "100%",
    },
    mediaDialogContainer: {
        maxHeight: 640,
        overflow: 'hidden',
        width: "100%",
    },

    price: {
        fontWeight: 'bold',
        fontSize: '1em',
        color: lightGreen[500]
    },
    priceDialog: {
        fontWeight: 'bold',
        fontSize: '1.3em',
        color: lightGreen[500],
        display: "flex",
        minWidth: "180px",
        justifyContent: 'space-between',
        alignItems: "baseline"
    },
    media: {
        objectFit: 'cover',
        height: 140,
        width: "100%",
    },
    imageContainer: {
        maxHeight: 140,
        overflow: 'hidden',
        width: "100%",
    },
    valueContainer: {
        overflowX: 'hidden',
    },
    title: {
        fontSize: '1.3em',
    },
    subTitle: {
        fontSize: '1em',
        display: "flex",
        justifyContent: 'space-between',
        alignItems: "baseline"
    },
    nSprints: {
        color: grey[500],
        whiteSpace: "nowrap"
    },
    dialogTitle: {
        display: 'flex',
        padding: '1em',
        height: '31px',
        justifyContent: 'space-between',
        alignItems: 'center',
    },
});

export default productCardStyle;
