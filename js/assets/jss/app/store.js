export const particlesImage = (options) => ({
    "fps_limit": 15,
    "particles": {
        "number": {
            "value": options.particles || 120,
            "density": {
                "enable": false
            }
        },
        "line_linked": {
            "enable": true,
            "distance": options.linkDistance || 40.80,
            "opacity": 0.5,
            "color": options.color || "#fff",
        },
        "move": {
            "speed": 0.6180,
            "direction": "random",
            "attract": {
                "enable": true,
                "rotateX": 2600,
                "rotateY": 18000
            }
        },
        "opacity": {
            "anim": {
                "enable": true,
                "opacity_min": 0.7,
                "speed": 2,
                "sync": false
            },
            "value": 1
        }
    },
    "polygon": {
        "enable": true,
        "scale": options.scale || 1,
        "type": "inline",
        "move": {
            "radius": 3.143,
            "path": "radius"
        },
        "url": options.image,
        "inline": {
            "arrangement": "equidistant"
        },
        "draw": {
            "enable": false,
            "stroke": {
                "color": "rgba(255, 255, 255, .2)",
                "width": 0.2
            }
        }
    },
    "retina_detect": false,
    "interactivity": {
        "events": {
            "onhover": {
                "enable": true,
                "mode": "bubble"
            },
            "onclick": {
                "enable": true,
                "mode": "push"
            }
        },
        "modes": {
            "bubble": {
                "size": 3,
                "opacity": 0.6180,
                "distance": 40
            }, "repulse": {
                "distance": 8
            },
            "push": {
                "particles_nb": 1
            }
        }
    }
});
export const particlesFree = () => ({
    "particles": {
        "number": {
            "value": 90,
            "density": {
                "enable": true,
                "value_area": 1500
            }
        },
        "line_linked": {
            "enable": true,
            "opacity": 0.618
        },
        "move": {
            "direction": "right",
            "speed": 0.218
        },
        "size": {
            "value": 1
        },
        "opacity": {
            "anim": {
                "enable": true,
                "speed": 1,
                "opacity_min": 0.7639
            }
        }
    },
    "interactivity": {
        "events": {
            "onclick": {
                "enable": true,
                "mode": "push"
            },
            "onhover": {
                "enable": true,
                "mode": "grab"
            },
        },
        "modes": {
            "push": {
                "particles_nb": 6
            },
            "grab": {
                "distance": 140,
                "line_linked": {
                    "opacity": 1
                }
            },
        }
    },
    "retina_detect": true

});


export const stylesApp = theme => ({
    root: {
        height: '100%',
    }, content: {
        width: '100%',
        height: '100%',
    },
    switchWrapper: {
        position: 'relative',
        height: '100%',
        '& >div': {
            width: '100%',
            height: '100%',

        }
    },
});


import {container, paralaxHeader, particles} from "../app";

const drawerWidth = 440;

export const StoreAppStyle = (theme) => ({
    container: {
        zIndex: "12",
        color: "#FFFFFF",
        ...container
    },
    subtitle: {
        fontSize: "1.313rem",
        maxWidth: "500px",
        margin: "10px auto 0"
    },
    main: {
        background: "#FFFFFF",
        position: "relative",
        zIndex: "3"
    },
    mainRaised: {
        margin: "-60px 30px 0px",
        borderRadius: "6px",
        boxShadow:
            "0 16px 24px 2px rgba(0, 0, 0, 0.14), 0 6px 30px 5px rgba(0, 0, 0, 0.12), 0 8px 10px -5px rgba(0, 0, 0, 0.2)"
    },
    containerUnsafe: {
        paddingRight: "0px",
        paddingLeft: "0px",
    },
    mainDistribution: {
        paddingRight: "0px",
        paddingLeft: "0px",
        margin: "-60px 30px 0px",
        justifyContent: 'space-between',
        background: "transparent"
    },
    ...paralaxHeader(theme),
    section: {
        padding: "20px 0",
        color: theme.palette.text.secondary,
    },
    mainCenter: {
        maxWidth: 880,
        marginLeft: "auto",
        marginRight: "auto"
    },
    containerHeader: {
        color: "#FFFFFF",
        zIndex: "12",
        ...container
    },
    headerType: {
        padding: 0,
        margin: 0
    },
    gridList: {
        paddingTop: 20,
    },
    btnHeader: {
        margin: "0 5px",
    },
    root: {
        height: "100%",
        width: "100%",
    },
    sectionProducts: {
        // background: "#FFFFFF",
        position: "relative",
        zIndex: "3",
        flex: '1',
        margin: "0px 30px",
        marginBottom: "61px",
        borderRadius: "6px",
        // boxShadow:
        //     "0 16px 24px 2px rgba(0, 0, 0, 0.14), 0 6px 30px 5px rgba(0, 0, 0, 0.12), 0 8px 10px -5px rgba(0, 0, 0, 0.2)",
        // backgroundColor: theme.palette.background.paper,
        ...container
    },
    sectionProductsFirst: {
        margin: "-60px auto 0px",
    },
    ...particles,
    headerDivider: {},

});
