export const ParallaxStyle = {
    parallax: {
        height: "90vh",
        maxHeight: "1000px",
        minHeight: "min-content",
        overflow: "hidden",
        position: "relative",
        backgroundPosition: "center center",
        backgroundSize: "cover",
        backgroundColor: "#050a0d",
        margin: "0",
        padding: "0",
        paddingBottom: "80px",
        paddingTop: "60px",
        transition: " min-height  1s ease",
        border: "0",
        display: "flex",
        alignItems: "center"
    },
    gallery:{
        opacity:"0.8",
    },
    filter: {
        "&:before": {
            background: "rgba(0, 0, 0, 0.5)"
        },
        "&:after,&:before": {
            position: "absolute",
            zIndex: "1",
            width: "100%",
            height: "100%",
            display: "block",
            left: "0",
            top: "0",
            content: "''"
        }
    },
    small: {
        height: "380px"
    }
};
