import {fromGlobalId, nodeDefinitions} from "graphql-relay";

export const createNodeDefinitionsWithRepositories = (collection) => {
    return nodeDefinitions(
        globalId => {
            const {type, id} = fromGlobalId(globalId);
            console.log(type);
            let findRepo = collection.findRepo(type);
            if (findRepo) {
                return findRepo.find(id).id;
            }
            return null
        },
        obj => {
            return collection.findRepoByObj(obj).getQL();
        })
};
