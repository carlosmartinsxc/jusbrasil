import {GraphQLBoolean, GraphQLFloat, GraphQLInt, GraphQLList, GraphQLObjectType, GraphQLString} from "graphql";
import {globalIdField} from "graphql-relay";
import {nodeInterface} from "../repositoryCollection";
import {GraphQLComment} from "../comment/comment.ql";
import {GraphQLValidationErrors} from "../helper";

const OwnerType = new GraphQLObjectType({
    name: 'Owner',
    fields: () => ({
        avatar: {type: GraphQLString},
        fullName: {type: GraphQLString},
    }),
});
export const GraphQLProduct = new GraphQLObjectType({
        name: 'Product',
        fields: {
            id: globalIdField('Product'),
            title: {
                type: GraphQLString,
                resolve: obj => obj.title,
            },
            owner: {
                type: OwnerType,
                resolve: obj => obj.owner
            },
            favorite: {
                type: GraphQLBoolean,
                resolve: obj => obj.favorite
            },
            comments: {
                type: GraphQLList(GraphQLComment),
                resolve: obj => obj.comments
            },
            totalComments: {
                type: GraphQLInt,
                resolve: obj => obj.comments ? obj.comments.length : 0,
            },
            description: {
                type: GraphQLString,
                resolve: obj => obj.description,
            },
            images: {
                type: GraphQLList(GraphQLString),
                resolve: obj => obj.images
            },
            material: {
                type: GraphQLString,
                resolve: obj => obj.material,
            },
            price: {
                type: GraphQLFloat,
                resolve: obj => obj.price,
            },
            stock: {
                type: GraphQLInt,
                resolve: obj => obj.stock,
            },
            commentErrors: {
                type: GraphQLList(GraphQLValidationErrors),
                resolve: obj => obj.commentErrors
            },
            totalInCart: {
                type: GraphQLInt,
                resolve: obj => obj.totalInCart
            },
        },
        interfaces: [nodeInterface]
    })
;