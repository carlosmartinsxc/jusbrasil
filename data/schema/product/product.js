import {nodeInterface} from "../../node.definitions";
import repositoryCollection from "../repositoryCollection";

export class Product {
    id;
    stock;
    comments;
    availableInStock() {
        let qnt = repositoryCollection
            .findRepo('ShoppingCart')
            .getCurrentShoppingCartInstance()
            .getQuantityProduct(this.id);
        let stock = this.stock;
        return stock > qnt;
    }

    getComments() {
        let commentRepo = repositoryCollection.findRepo('Comment');
        let newComments = [];
        this.comments.forEach(commentId => {
            let comment = commentRepo.find(commentId);
            if (comment) {
                newComments.push(commentRepo.find(commentId, {format: true}));
            }
        });
        return newComments;
    }


}
