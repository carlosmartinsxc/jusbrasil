import {nodeInterface} from "../../node.definitions";
import {Repo} from "../repo";
import repositoryCollection from "../repositoryCollection";
import {Product} from "./product";

export class ProductRepo extends Repo {
    alias = "Product";

    getInstanceObject() {
        return Product;
    }

    getQL() {
        return require('./product.ql').GraphQLProduct;
    }

    formatItem(item) {
        return {
            ...item,
            comments: this.findInstance(item.id).getComments(),//item.comments,// ,
            favorite: repositoryCollection
                .findRepo('User')
                .getCurrentUserInstance()
                .getFavoriteIndex(item.id) !== -1,
            totalInCart: repositoryCollection.findRepo('ShoppingCart')
                .getCurrentShoppingCartInstance()
                .getQuantityProduct(item.id)
        };
    }


}
