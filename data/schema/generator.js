import faker from "faker";

export class Generator {
    constructor(repositoryCollection) {
        this.repositoryCollection = repositoryCollection;
        this.loaders = [];
        let userLoader = new UserLoader(repositoryCollection);
        let shoppingCartLoader = new ShoppingCartLoader(repositoryCollection);
        let commentLoader = new CommentLoader(repositoryCollection);
        let productLoader = new ProductLoader(repositoryCollection);
        this.addLoader(userLoader);
        this.addLoader(shoppingCartLoader);
        this.addLoader(productLoader);
        this.addLoader(commentLoader);
    }

    addLoader(loader) {
        this.loaders.push(loader)
    }

    generate() {
        this.loaders.forEach(loader => {
            loader.generateItems();
        })
    }
}

class Loader {
    constructor(repositoryCollection) {
        this.repositoryCollection = repositoryCollection;
    }

    addItem(id) {
        this.getRepository().addItem(id, this.getSerializeItem())
    }

    generateId() {
        return faker.random.number({min: 0, max: 10000})
    }

    getSerializeItem() {
        return undefined;
    }

    getRepository() {
        return undefined;
    }
}


class UserLoader extends Loader {
    getRepository() {
        return this.repositoryCollection.findRepo('User')
    }

    getSerializeItem() {
        return {
            name: faker.finance.accountName(),
            avatar: faker.image.avatar(),
            favorites: [],
            likeComments: []
        };
    }

    generateItems() {
        this.addItem(1);
    }

}

class ProductLoader extends Loader {
    getRepository() {
        return this.repositoryCollection.findRepo('Product')
    }

    getSerializeItem() {
        let commentRepo = this.repositoryCollection.findRepo('Comment');
        let commentLoader = new CommentLoader();
        let comments = [];
        let maxComments = faker.random.number({min: 0, max: 10});
        for (let i = 1; i < maxComments; i++) {
            let generateId = this.generateId();
            commentRepo.addItem(generateId, commentLoader.getSerializeItem());
            comments.push(generateId);
        }
        return {
            title: faker.company.catchPhraseDescriptor(),
            owner: {
                avatar: faker.image.avatar(),
                fullName: faker.name.firstName(null) + faker.name.lastName(null)
            },
            description: faker.commerce.productAdjective(),
            images: [
                faker.image[faker.random.arrayElement(["food", "nightlife", "fashion"])](300, 300, true),
            ],
            comments: comments,
            material: faker.commerce.productMaterial(),
            price: faker.commerce.price(0, 25),
            stock: faker.random.number({
                'min': 6,
                'max': 16
            })
        };
    }

    generateItems() {
        for (let i = 1; i < 30; i++) {
            this.addItem(i);
        }
    }

}

class ShoppingCartLoader extends Loader {
    getRepository() {
        return this.repositoryCollection.findRepo('ShoppingCart')
    }

    generateItems() {
        this.addItem(1, this.getSerializeItem());
    }

    getSerializeItem() {
        return {
            totalItems: 0,
            products: [],
        };
    }

}

class CommentLoader extends Loader {
    getRepository() {
        return this.repositoryCollection.findRepo('Comment')
    }

    generateItems() {
        this.addItem(1, this.getSerializeItem());
    }

    getSerializeItem() {
        return {
            comment: faker.company.catchPhraseDescriptor(),
            createdAt: faker.date.between(new Date(2018, 1), new Date()),
            editor: {
                avatar: faker.image.avatar(),
                fullName: faker.name.firstName(null) + faker.name.lastName(null)
            },
        };
    }

}
