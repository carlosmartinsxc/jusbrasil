// Mock user data
import {GraphQLFloat, GraphQLInt, GraphQLList, GraphQLObjectType} from "graphql";
import {GraphQLProduct} from "../product/product.ql";
import {globalIdField} from "graphql-relay";
import repositoryCollection, {nodeInterface} from "../repositoryCollection";

let GraphQLStoreProduct = new GraphQLObjectType({
    name: 'ShoppingCartProduct',
    fields: () => ({
        product: {
            type: GraphQLProduct,
            resolve: obj => obj.product
        },
        qnt: {type: GraphQLInt},
    }),
});
export const GraphQLShoppingCart = new GraphQLObjectType({
    name: 'ShoppingCart',
    fields: {
        id: globalIdField('ShoppingCart'),
        products: {
            type: new GraphQLList(GraphQLStoreProduct),
            resolve: (obj) => {
                return obj.products;
            }
        },
        totalItems: {
            type: GraphQLInt,
            resolve: obj => repositoryCollection.findRepo('ShoppingCart')
                .getCurrentShoppingCartInstance()
                .getTotal()
        },
        totalPrice: {
            type: GraphQLFloat,
            resolve: obj => repositoryCollection.findRepo('ShoppingCart')
                .getCurrentShoppingCartInstance()
                .getTotalPrice()
        },
    },
    interfaces: [nodeInterface]
});


