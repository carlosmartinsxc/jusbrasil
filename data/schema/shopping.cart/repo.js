// Mock user data
import {Repo} from "../repo";
import repositoryCollection from "../repositoryCollection";
import {ShoppingCart} from "./shopping.cart";

export class CartRepo extends Repo {
    alias = "ShoppingCart";

    getInstanceObject() {
        return ShoppingCart;
    }
    formatItem(item) {
        let instanceCart = this.getCurrentShoppingCartInstance();
        return {
            ...item,
            products: instanceCart.getProducts(),
            totalInCart: instanceCart.getTotal(),
            totalPrice: instanceCart.getTotalPrice()
        };
    }

    getQL() {
        return require('./cart.ql').GraphQLShoppingCart;
    }

    getFindQLObject = () => {
        return {
            type: this.getQL(),
            resolve: (props) => {
                return this.find(
                    this.getCurrentShoppingCart().id,
                    {format: true}
                );
            },
        }
    }

    getCurrentShoppingCart() {
        let currentUserID = repositoryCollection
            .findRepo('User')
            .getCurrentUser()
            .id;
        return this.find(currentUserID);
    }

    getCurrentShoppingCartInstance() {
        return this.createInstanceWithData(this.getCurrentShoppingCart());
    }

    checkInStock(productId) {
        let inStock = repositoryCollection
            .findRepo('Product')
            .findInstance(productId)
            .availableInStock();
        if (!inStock) {
            return {
                errors: [{
                    field: 'stock',
                    message: 'out of stock'
                }]
            }
        }
        return false;
    }


    addProductCart(productId) {
        let errors = this.checkInStock(productId);
        let currentCart = this.getCurrentShoppingCartInstance();
        if (!errors && currentCart.hasProduct(productId)) {
            let cartItemId = currentCart.increaseQuantity(productId);
            return {cartItemId};
        }else if (!errors) {
            currentCart.addProduct(productId)
        }
        return {errors};

    }

    decreaseProductCart(productId) {
        let currentCart = this.getCurrentShoppingCartInstance();
        if (currentCart.hasProduct(productId)) {
            currentCart.decreaseQuantity(productId)
        }
    }

    removeProductCart(productId) {
        let currentCart = this.getCurrentShoppingCartInstance();
        if (currentCart.hasProduct(productId)) {
            currentCart.removeProduct(productId)
        }
    }

}


