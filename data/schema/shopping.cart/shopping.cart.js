// Mock user data
import repositoryCollection from "../repositoryCollection";

export class ShoppingCart {
    products = [];

    getQuantityProduct(productId) {
        if (this.products.length === 0) {
            return 0;
        } else {
            let productCart = this.products.find(
                cartProduct => parseInt(cartProduct.product) === parseInt(productId)
            );
            return productCart ? productCart.qnt : 0;
        }
    }

    queryProduct(productId) {
        return val => parseInt(val.product) === parseInt(productId);
    }

    getProduct(productId, index = false) {
        if (index) {
            return this.products.findIndex(this.queryProduct(productId))
        }
        return this.products.find(this.queryProduct(productId))
    }

    removeProduct(productId) {
        let index = this.getProduct(productId, true);
        this.products.splice(index, 1);
    }

    increaseQuantity(productId) {
        let productStore = this.getProduct(productId);
        productStore.qnt += 1;
        return productStore;
    }

    decreaseQuantity(productId) {
        let productStore = this.getProduct(productId);
        if (productStore.qnt > 1) {
            productStore.qnt -= 1;
        }
        return productStore;
    }

    hasProduct(productId) {
        return this.getProduct(productId, true) >= 0;
    }

    addProduct(productId) {
        this.products.push({
            product: productId,
            qnt: 1
        });
        return null;
    }


    getTotal() {
        return this.products.map(product => product.qnt)
            .reduce((a, b) => a + b, 0);
    }


    getTotalPrice() {
        let productRepo = repositoryCollection.findRepo('Product');
        return this.products.map(product => {
            return product.qnt * productRepo.find(product.product).price;
        })
            .reduce((a, b) => a + b, 0);
    }

    getProducts() {
        let repo = repositoryCollection.findRepo('Product');
        let productsData = this.products.map(productStore => {
            return {
                ...productStore,
                product: {
                    ...repo.find(productStore.product, {
                        format: false
                    }),
                    totalInCart: this.getQuantityProduct(productStore.product)
                },
            }
        });
        return productsData ? productsData : [];
    }
}
