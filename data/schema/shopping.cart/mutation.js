// Mock user data
import {GraphQLID, GraphQLNonNull} from "graphql";
import {mutationWithClientMutationId} from "graphql-relay";
import repositoryCollection from "../repositoryCollection";
import {fromGlobalId} from "graphql-relay/lib/node/node";

export const GraphQLAddCartItemMutation = mutationWithClientMutationId({
    name: 'AddCartItem',
    inputFields: {
        productId: {type: new GraphQLNonNull(GraphQLID)},
    },
    outputFields: {
        cartEdge: repositoryCollection
            .findRepo('ShoppingCart')
            .getCreateQLObject('cartId'),
    },
    mutateAndGetPayload: ({productId}) => {
        productId = fromGlobalId(productId).id;
        let shoppingCartRepo = repositoryCollection
            .findRepo('ShoppingCart');

        let cartId = shoppingCartRepo.getCurrentShoppingCart().id;
        let {errors} = shoppingCartRepo
            .addProductCart(productId);

        return {
            cartId: cartId,
            errors: errors || []
        };
    },
});


export const GraphQLDecreaseCartItemMutation = mutationWithClientMutationId({
    name: 'DecreaseCartItem',
    inputFields: {
        productId: {type: new GraphQLNonNull(GraphQLID)},
    },
    outputFields: {
        cartEdge: repositoryCollection
            .findRepo('ShoppingCart')
            .getFindQLObject(),
    },
    mutateAndGetPayload: ({productId}) => {
        productId = fromGlobalId(productId).id;
        repositoryCollection
            .findRepo('ShoppingCart')
            .decreaseProductCart(productId);
        return {productId}
    },
});


export const GraphQLRemoveCartItemMutation = mutationWithClientMutationId({
    name: 'RemoveCartItem',
    inputFields: {
        productId: {type: new GraphQLNonNull(GraphQLID)},
    },
    outputFields: {
        cartEdge: repositoryCollection
            .findRepo('ShoppingCart')
            .getFindQLObject(),
    },
    mutateAndGetPayload: ({cartRepoId, productId}) => {
        productId = fromGlobalId(productId).id;
        repositoryCollection
            .findRepo('ShoppingCart')
            .removeProductCart(productId);

        return {productId};
    },
});


