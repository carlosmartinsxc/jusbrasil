import Validator from "fastest-validator";
import {GraphQLList} from "graphql";

export class Repo {
    items = {};
    schemaValidator = {};

    getInstanceObject() {
        new Error('abstract method')
    };

    getList() {
        return this.shuffleItems(this.items);
    }

    getLimit() {
        return 20;
    }

    shuffleItems(items) {
        let productKeys = Object.keys(items).slice(0, this.getLimit());
        this.randomize(productKeys);
        return productKeys.map(key => this.find(key, {format: true}));
    }

    randomize(productKeys) {
        productKeys.sort(function (a, b) {
            return 0.61803 - Math.random()
        })
    }

    find(id, options) {
        let item = this.items[parseInt(id)];
        if (options && options.format === true) {
            return this.formatItem(item);
        }
        return item;
    }

    findInstance(id) {
        let instance = this.createInstanceWithData(this.find(id))
        return instance;
    }

    validate(data) {
        let v = new Validator();
        return v.validate(data, this.schemaValidator)
    }

    formatItem(item) {
        return item;
    }

    getFindQLObject = (idFieldName) => {
        return {
            type: this.getQL(),
            resolve: (props) => {
                return this.find(props[idFieldName], {format: true});
            },
        }
    };
    getListQLObject = () => {
        return {
            type: new GraphQLList(this.getQL()),
            resolve: () => this.getList(),
        }
    };
    getCreateQLObject = (keyName) => {
        return {
            type: this.getQL(),
            resolve: ({errors, ...props}) => {
                errors = errors && errors.length ? errors : false;
                return {
                    ...this.find(props[keyName], {format: errors === false}),
                    errors: errors
                }
            },
        }
    };

    addItem(id, entityData) {
        if (this.items[id]) {
            return null;
        }
        let item = this.createInstanceWithData(entityData);
        item.id = parseInt(id);
        this.items[id] = item;
        return item;
    }

    createInstanceWithData(entityData) {
        const Instance = this.getInstanceObject();
        let item = new Instance;
        Object.keys(entityData).map(keyName => {
            item[keyName] = entityData[keyName];
        });
        return item;
    }
}

