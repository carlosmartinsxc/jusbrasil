import {GraphQLObjectType, GraphQLString} from "graphql";
import faker from "faker";

export const GraphQLValidationErrors = new GraphQLObjectType({
    name: 'ValidationErrorsType',
    fields: () => ({
        field: {type: GraphQLString},
        message: {type: GraphQLString},
    }),
});

export function generateId() {
    return faker.random.number({min: 0, max: 10000});
}
