import {RepositoryCollection} from "./repository.collection";
import {createNodeDefinitionsWithRepositories} from "../node.definitions";
import {UserRepo} from "./user/repo";
import {CartRepo} from "./shopping.cart/repo";
import {ProductRepo} from "./product/repo";
import {CommentRepo} from "./comment/repo";

const repositoryCollection = new RepositoryCollection();
repositoryCollection.addRepo(new UserRepo());
repositoryCollection.addRepo(new CartRepo());
repositoryCollection.addRepo(new ProductRepo());
repositoryCollection.addRepo(new CommentRepo());
let nodeDefinitions = createNodeDefinitionsWithRepositories(repositoryCollection);
export const {nodeInterface, nodeField} = nodeDefinitions;
// repositoryCollection.applyInterfaces(nodeInterface);
export default repositoryCollection;