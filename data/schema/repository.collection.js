export class RepositoryCollection {
    constructor() {
        this.repositories = {};
    }

    addRepo(repository) {
        if (!this.repositories[repository.alias]) {
            this.repositories[repository.alias] = repository;
        }
    }

    findRepo(repositoryAlias) {
        return this.repositories[repositoryAlias];
    }

    findRepoByObj(object) {
        let repoFound = null;
        this.getRepositoryArray().find(repo => {
            return object instanceof repo.getInstanceObject();
        });
        return repoFound;
    }

    getRepositoryArray() {
        return Object.keys(this.repositories)
            .map(repo => this.repositories[repo]);
    }
}