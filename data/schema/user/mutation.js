import {GraphQLID, GraphQLNonNull} from "graphql";
import {mutationWithClientMutationId} from "graphql-relay";
import repositoryCollection from "../repositoryCollection";
import {fromGlobalId} from "graphql-relay/lib/node/node";

export const GraphQLToggleFavoriteMutation = mutationWithClientMutationId({
    name: 'ToggleFavoriteMutation',
    inputFields: {
        productId: {type: new GraphQLNonNull(GraphQLID)},
    },
    outputFields: {
        userEdge: repositoryCollection
            .findRepo('User')
            .getFindQLObject(),
        productEdge: repositoryCollection
            .findRepo('Product')
            .getFindQLObject('productId'),
    },
    mutateAndGetPayload: ({productId}) => {
        productId = fromGlobalId(productId).id;
        repositoryCollection
            .findRepo('User')
            .toggleFavorite(productId);
        return {productId};
    },
});




