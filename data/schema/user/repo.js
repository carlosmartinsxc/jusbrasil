// Mock user data
import {Repo} from "../repo";
import {User} from "./user";

export class UserRepo extends Repo {
    alias = "User";

    getQL() {
        return require('./user.ql').GraphQLUser;
    }

    getInstanceObject() {
        return User;
    }

    getFindQLObject = () => {
        return {
            type: this.getQL(),
            resolve: (props) => {
                return this.find(
                    this.getCurrentUser().id,
                    {format: true}
                );
            },
        }
    };

    getCurrentUser() {
        return this.items[1];
    }

    getCurrentUserInstance() {
        return this.createInstanceWithData(this.getCurrentUser());
    }

    toggleComment(commentId) {
        this.getCurrentUserInstance().toggleComment(commentId);
    }

    toggleFavorite(productId) {
        this.getCurrentUserInstance().toggleFavorite(productId);
    }

}
