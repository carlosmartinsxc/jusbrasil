// Mock user data

export class User {
    favorites;
    likeComments;
    getFavorites() {
        return this.favorites || [];
    }

    getLikeComments() {
        return this.likeComments || [];
    }

    getFavoriteIndex(productId) {
        return this.getFavorites()
            .findIndex(favId => parseInt(favId) === parseInt(productId));
    }

    toggleComment(productId) {
        let indexComment = this.getLikeCommentIndex(productId);
        if (indexComment !== -1) {
            this.getLikeComments().splice(indexComment, 1);
        } else {
            this.getLikeComments().push(productId);
        }
    }

    getLikeCommentIndex(commentSearchId) {
        return this.getLikeComments()
            .findIndex(commentId => parseInt(commentSearchId) === parseInt(commentId));
    }

    toggleFavorite(productId) {
        let likeCommentsIndex = this.getLikeCommentIndex();
        if (likeCommentsIndex >= 0) {
            this.getFavorites().splice(likeCommentsIndex, 1);
        } else {
            this.getFavorites().push(productId);
        }
    }
}
