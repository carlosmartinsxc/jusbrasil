// Mock user data
import {globalIdField} from "graphql-relay";
import {GraphQLID, GraphQLObjectType, GraphQLString} from "graphql";
import {GraphQLList} from "graphql/type/definition";
import {nodeInterface} from "../repositoryCollection";

export const GraphQLUser = new GraphQLObjectType({
    name: 'User',
    fields: {
        id: globalIdField('User'),
        name: {
            type: GraphQLString,
            resolve: obj => obj.name,
        },
        avatar: {
            type: GraphQLString,
            resolve: obj => obj.avatar,
        },
        favorites: {
            type: GraphQLList(GraphQLID),
            resolve: user => user.favorites || [],
            description: 'User Favorites',
        },
        likeComments: {
            type: GraphQLList(GraphQLID),
            resolve: user => user.likeComments || [],
            description: 'User likeComments',
        },

    },
    interfaces: [nodeInterface]
});
