import {nodeInterface} from "../../node.definitions";
import {Repo} from "../repo";
import repositoryCollection from "../repositoryCollection";
import {generateId} from "../helper";
import {Comment} from "./comment";


export class CommentRepo extends Repo {
    alias = "Comment";
    schemaValidator = {
        comment: {type: "string", min: 2, max: 255},
    };

    getInstanceObject() {
        return Comment;
    }

    getQL() {
        return require('./comment.ql').GraphQLComment;
    }

    addComment(text, productId) {
        let validateData = this.validate({comment: text});
        if (validateData === true) {
            let product = repositoryCollection
                .findRepo('Product')
                .find(productId);
            let currentUser = repositoryCollection
                .findRepo('User')
                .getCurrentUser();
            let comment = this.addItem(generateId(), {
                comment: text,
                editor: {
                    avatar: currentUser.avatar,
                    fullName: `${currentUser.name} `
                }
            });
            let comments = product.comments || [];
            comments.push(comment.id);
            return comment;
        }
        return {errors: validateData};
    }

    formatItem(item) {
        return {
            ...item,
            likeComment: repositoryCollection
                .findRepo('User')
                .getCurrentUser()
                .getLikeCommentIndex(item.id) !== -1,
        };
    }
}
