import {GraphQLID, GraphQLNonNull, GraphQLString} from "graphql";
import {fromGlobalId, mutationWithClientMutationId} from "graphql-relay";
import repositoryCollection from "../repositoryCollection";

export const GraphQLToggleCommentLikeMutation = mutationWithClientMutationId({
    name: 'ToggleCommentLikeMutation',
    inputFields: {
        commentId: {type: new GraphQLNonNull(GraphQLID)},
    },
    outputFields: {
        commentEdge: repositoryCollection
            .findRepo('Comment').getFindQLObject('commentId'),
    },
    mutateAndGetPayload: ({commentId}) => {
        commentId = fromGlobalId(commentId).id;
        repositoryCollection
            .findRepo('User')
            .toggleComment(commentId);
        return {commentId};
    },
});


export const GraphQLAddCommentProductMutation = mutationWithClientMutationId({
    name: 'AddCommentProductMutation',
    inputFields: {
        productId: {type: new GraphQLNonNull(GraphQLID)},
        text: {type: new GraphQLNonNull(GraphQLString)},
    },
    outputFields: {
        commentEdge: repositoryCollection
            .findRepo('Comment')
            .getCreateQLObject('commentId'),
    },
    mutateAndGetPayload: ({text, productId}) => {
        productId = fromGlobalId(productId).id;
        let comment = repositoryCollection
            .findRepo('Comment')
            .addComment(text, productId);
        let commentId = comment.id;
        if (comment.errors) {
            let errors = comment.errors;
            return {errors, productId, commentId}
        }
        return {productId, commentId};
    },
});



