import {GraphQLBoolean, GraphQLList, GraphQLObjectType, GraphQLString} from "graphql";
import {globalIdField} from "graphql-relay";
import {nodeInterface} from "../repositoryCollection";
import {GraphQLValidationErrors} from "../helper";

const CommentEditorType = new GraphQLObjectType({
    name: 'CommentEditorType',
    fields: () => ({
        avatar: {type: GraphQLString},
        fullName: {type: GraphQLString},
    }),
});

export const GraphQLComment = new GraphQLObjectType({
        name: 'Comment',
        fields: {
            id: globalIdField('Comment'),
            comment: {
                type: GraphQLString,
                resolve: obj => obj.comment,
            },
            likeComment: {
                type: GraphQLBoolean,
                resolve: obj => obj.likeComment,
            },
            editor: {
                type: CommentEditorType,
                resolve: obj => obj.editor
            },
            errors: {
                type: GraphQLList(GraphQLValidationErrors),
                resolve: obj => obj.errors || []
            },

        },
        interfaces: [nodeInterface]
    })
;