input AddCartItemInput {
  productId: ID!
  clientMutationId: String
}

type AddCartItemPayload {
  cartEdge: ShoppingCart
  clientMutationId: String
}

input AddCommentProductMutationInput {
  productId: ID!
  text: String!
  clientMutationId: String
}

type AddCommentProductMutationPayload {
  commentEdge: Comment
  clientMutationId: String
}

type Comment implements Node {
  """The ID of an object"""
  id: ID!
  comment: String
  likeComment: Boolean
  editor: CommentEditorType
  errors: [ValidationErrorsType]
}

type CommentEditorType {
  avatar: String
  fullName: String
}

input DecreaseCartItemInput {
  productId: ID!
  clientMutationId: String
}

type DecreaseCartItemPayload {
  cartEdge: ShoppingCart
  clientMutationId: String
}

type Mutation {
  addCartItem(input: AddCartItemInput!): AddCartItemPayload
  decreaseCartItem(input: DecreaseCartItemInput!): DecreaseCartItemPayload
  removeCartItem(input: RemoveCartItemInput!): RemoveCartItemPayload
  toggleFavorite(input: ToggleFavoriteMutationInput!): ToggleFavoriteMutationPayload
  toggleCommentLike(input: ToggleCommentLikeMutationInput!): ToggleCommentLikeMutationPayload
  addCommentProduct(input: AddCommentProductMutationInput!): AddCommentProductMutationPayload
}

"""An object with an ID"""
interface Node {
  """The id of the object."""
  id: ID!
}

type Owner {
  avatar: String
  fullName: String
}

type Product implements Node {
  """The ID of an object"""
  id: ID!
  title: String
  owner: Owner
  favorite: Boolean
  comments: [Comment]
  totalComments: Int
  description: String
  images: [String]
  material: String
  price: Float
  stock: Int
  commentErrors: [ValidationErrorsType]
  totalInCart: Int
}

type Query {
  user: User
  cart: ShoppingCart
  products: [Product]

  """Fetches an object given its ID"""
  node(
    """The ID of an object"""
    id: ID!
  ): Node
}

input RemoveCartItemInput {
  productId: ID!
  clientMutationId: String
}

type RemoveCartItemPayload {
  cartEdge: ShoppingCart
  clientMutationId: String
}

type ShoppingCart implements Node {
  """The ID of an object"""
  id: ID!
  products: [ShoppingCartProduct]
  totalItems: Int
  totalPrice: Float
}

type ShoppingCartProduct {
  product: Product
  qnt: Int
}

input ToggleCommentLikeMutationInput {
  commentId: ID!
  clientMutationId: String
}

type ToggleCommentLikeMutationPayload {
  commentEdge: Comment
  clientMutationId: String
}

input ToggleFavoriteMutationInput {
  productId: ID!
  clientMutationId: String
}

type ToggleFavoriteMutationPayload {
  userEdge: User
  productEdge: Product
  clientMutationId: String
}

type User implements Node {
  """The ID of an object"""
  id: ID!
  name: String
  avatar: String

  """User Favorites"""
  favorites: [ID]

  """User likeComments"""
  likeComments: [ID]
}

type ValidationErrorsType {
  field: String
  message: String
}
