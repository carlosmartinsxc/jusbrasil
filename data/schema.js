import {GraphQLObjectType, GraphQLSchema,} from 'graphql';
import repositoryCollection, {nodeField} from "./schema/repositoryCollection";
import {GraphQLToggleFavoriteMutation} from "./schema/user/mutation";
import {
    GraphQLAddCartItemMutation,
    GraphQLDecreaseCartItemMutation,
    GraphQLRemoveCartItemMutation
} from "./schema/shopping.cart/mutation";
import {GraphQLAddCommentProductMutation, GraphQLToggleCommentLikeMutation} from "./schema/comment/mutation";
import {Generator} from "./schema/generator";


let dataGenerator = new Generator(repositoryCollection);
dataGenerator.generate();


const Query = new GraphQLObjectType({
    name: 'Query',
    fields: {
        user: repositoryCollection.findRepo('User').getFindQLObject(),
        cart: repositoryCollection.findRepo('ShoppingCart').getFindQLObject(),
        products: repositoryCollection.findRepo('Product').getListQLObject(),
        node: nodeField,
    },
});

const Mutation = new GraphQLObjectType({
    name: 'Mutation',
    fields: {
        //Shopping Cart
        addCartItem: GraphQLAddCartItemMutation,
        decreaseCartItem: GraphQLDecreaseCartItemMutation,
        removeCartItem: GraphQLRemoveCartItemMutation,
        // User
        toggleFavorite: GraphQLToggleFavoriteMutation,
        // Products
        // myCartProducts: GraphQLMyCartProductsMutation,
        // generateProducts: GraphQLGenerateProductsMutation,
        // Comments
        toggleCommentLike: GraphQLToggleCommentLikeMutation,
        addCommentProduct: GraphQLAddCommentProductMutation,

    },
});

export const schema = new GraphQLSchema({
    query: Query,
    mutation: Mutation,
});